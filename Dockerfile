FROM cdrx/pyinstaller-windows

RUN apt-get update -qy \
	&& apt-get install -y 

CMD pip install boto3==1.3.0